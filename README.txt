mojo_boilerplate Extension
==========================

HTML5 Boilerplate is the professional badass's base HTML/CSS/JS template for a fast, robust and future-proof site. 


After more than two years in iterative development, you get the best of the best practices baked in: cross-browser 
normalization, performance optimizations, even optional features like cross-domain Ajax and Flash. A starter apache 
.htaccess config file hooks you the eff up with caching rules and preps your site to serve HTML5 video, use @font-face, 
and get your gzip zipple on. Boilerplate is not a framework, nor does it prescribe any philosophy of development, it's 
just got some tricks to get your project off the ground quickly and right-footed.

This TYPO3 Extension tries to bring some of the power packages as an easy to use solution for your website.

Documentation
=============

For more information about HTML5 Boilerplate and the technical background please visit the homepage:
http://html5boilerplate.com/

Usage
=====

The extension hooks into the TYPO3 page rendering and changes some default options which are not
easily modified using typoscript.

Dependencies
============

It should work on all TYPO3 v4.x version. Only TYPO3 4.5 or newer are tested and supported.

Maintainers
===========

The mojo_boilerplate extensions is maintained by Morton Jonuschat <yabawock@gmail.com>.
HTML5 Boilerplate is brought to you by Paul Irish, Divya Manian and contributors.

Project Info
============

mojo_boilerplate is hosted on GitHub: https://bitbucket.org/mjonuschat/mojo_boilerplate where your
contributions, forkings, comments and feedback are greatly welcomed.

Credits
=======

Copyright ©2010-2012 Morton Jonuschat, released under the GPLv2 license.

page.boilerplate {
  enable {
    conditional_css = 1
    modernizr = 1
    base_css = 1
    short_charset = 1
    selectivizr = 1
    mediaqueries = 1
    jqueryui = 1
    jquery = 1
    chrome_frame = 1
  }
  external {
    jquery = 1
    jqueryui = 1
  }
  meta {
    ua_compatible = IE=edge,chrome=1
    viewport = width=device-width
  }

  options {
    google_analytics = UA-XXXXX-X
  }
}